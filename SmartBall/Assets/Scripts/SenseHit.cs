﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SenseHit : MonoBehaviour
{
	public int MY_NUM;
	private GameObject Base;
	private ControlBase Control_base;

    // Start is called before the first frame update
    void Start()
    {
		Base = GameObject.Find("Main Camera");
		Control_base = Base.GetComponent<ControlBase>();//enumを使いたいため
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "SPHERE")
		{
			ControlBase.HOLES my_num = (ControlBase.HOLES)MY_NUM;
			//エフェクト
			//効果音
			Control_base.Sense_sphere_fallen(my_num,collision);//陥没検知呼び出し
		}
	}
}
