﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadHiScore : MonoBehaviour
{
	private Text Text_hi_score;//ハイスコア表示用テキスト

    // Start is called before the first frame update
    void Start()
    {
		DataManager data_manager = GameObject.Find("Main Camera").GetComponent<DataManager>();
		int _hi_score = data_manager.read_hi_score();//ハイスコアを取得

		Text_hi_score = GameObject.Find("TextHiScore").GetComponent<Text>();
		Text_hi_score.text = "ハイスコア："+_hi_score;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
