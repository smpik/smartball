﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlQue : MonoBehaviour
{
	/*==============================================================================================*/
	/* 外部変数宣言(Inspectorから設定する)
	/*==============================================================================================*/
	public float SCALE_CONVERT;   //画面のスワイプ量⇔Hitterの移動量間の変換用
	public float THRESHOLD_SWIPE;   //スワイプ判定用閾値、これより少ないスワイプ量はスワイプとみなさない(←引っ掛かりポイント
    public float LIMIT_UPPER_POS_Y; //スワイプ量の上限、これ以上にはスワイプさせない
    public float LIMIT_LOWER_POS_Y; //スワイプ量の下限、これ以下にはスワイプさせない
    public float BANE;  //ばね定数
	public float SPEED_RETURN;//帰還するときの速度

	public GameObject Que;
    public Rigidbody Rb_Que;
	public Rigidbody Rb_Sphere;

	/*==============================================================================================*/
	/* 内部変数宣言																					*/
	/*==============================================================================================*/
	private float Pos_swipe_start = 0f;	//スワイプ時のタップ開始位置を記憶
	private float Pos_swipe_before = 0f;	//スワイプ中の前回指が触れていた位置
	private float Pos_swipe_now = 0f;		//現在のスワイプ位置

	private Vector3 Pos_origin_que;//Queのグローバル初期位置
	private Vector3 Pos_now_que;//Queのグローバル現在地
	private QUE_STATE Que_state;//キューの状態、これにID_STATEたちを入れて使う

	private bool Flg_touch = false;//指が触れてるか否かを示す

	/*==============================================================================================*/
	/* 内部定数宣言																					*/
	/*==============================================================================================*/
	private const float ZERO = 0.0f;    //ゼロ定数(井上先生が大好きな)
	private const float MARGIN_FOR_POS_ORIGIN = 0.001f;//初期位置判定に用いる（なぜか実行時にキューの位置が動いてしまうため)

	private enum TAP_STATE	//タッチの状況
	{
		NO = 0,//指が触れてない
		TOUCHED,//指が触れたとき
		KEEP,//指が触れたまま
		RELEASE,//指を話したとき
	}

	private enum POS_STATE	//キューの位置の状況
	{
		ORIGIN = 0,
		OTHER,
		LIMIT_UPPER,
	}
	/* キューに対するイベントインデックスの宣言 */
	private enum EVENTS	//遷移条件を示すインデックスたち
	{
		TAP_NO = 0,	//タップなし、画面に触れてない
		TOUCH,	//タップした、画面に触れた
		SWIPE,		//スワイプ、タップし続けている
		SWIPE_FAILED,//スワイプを失敗、タップした場所のまま指を離した
		SWIPE_END,	//スワイプ終了、スワイプした後に指を離した
		SWING_END,	//スイング終了
		RETURN_END,	//帰還
		x,//定義なし
		EVENT_NUM,
	}

	/* キューの状態インデックスの宣言 */
	private enum QUE_STATE		//キューの状態を表すインデックスたち
	{
		STANDBY = 0,	//静止状態
		CONTROLED,	//操作状態
		SWING,		//スイング状態
		RETURN,		//帰還状態
		STATE_NUM,
	}

	/* 静止状態用のイベント決定テーブルの宣言 */
	EVENTS[,] Event_for_standby_tbl =
	{	//NO			//TOUCHED		//KEEP	//RELEASE
		{EVENTS.TAP_NO,	EVENTS.TOUCH,	EVENTS.x,	EVENTS.TAP_NO},//ORIGIN
		{EVENTS.x,		EVENTS.x,		EVENTS.x,	EVENTS.x},//OTHER
		{EVENTS.x,		EVENTS.x,		EVENTS.x,	EVENTS.x},//LIMIT_UPPER
	};
	/* 操作状態用のイベント決定テーブルの宣言 */
	EVENTS[,] Event_for_controled_tbl =
	{	//NO		//TOUCHED	//KEEP			//RELEASE
		{EVENTS.x,	EVENTS.SWIPE,	EVENTS.SWIPE,	EVENTS.SWIPE_FAILED},//ORIGIN	//本当は操作状態にタッチなんてできないけど、チャタリングしちゃうみたい
		{EVENTS.x,	EVENTS.SWIPE,	EVENTS.SWIPE,	EVENTS.SWIPE_END},//OTHER
		{EVENTS.x,	EVENTS.x,	EVENTS.x,		EVENTS.x},//LIMIT_UPPER
	};
	/* スイング状態用のイベント決定テーブルの宣言 */
	EVENTS[,] Event_for_swing_tbl =
	{	//NO				//TOUCHED	//KEEP	//RELEASE
		{EVENTS.x,			EVENTS.x,	EVENTS.x,	EVENTS.x},//ORIGIN
		{EVENTS.x,			EVENTS.x,	EVENTS.x,	EVENTS.x},//OTHER
		{EVENTS.SWING_END,	EVENTS.x,	EVENTS.x,	EVENTS.x},//LIMIT_UPPER
	};
	/* 帰還状態用のイベント決定テーブルの宣言 */
	EVENTS[,] Event_for_return_tbl =
	{	//NO				//TOUCHED	//KEEP		//RELEASE
		{EVENTS.RETURN_END,	EVENTS.x,	EVENTS.x,	EVENTS.x},//ORIGIN
		{EVENTS.x,			EVENTS.x,	EVENTS.x,	EVENTS.x},//OTHER
		{EVENTS.x,			EVENTS.x,	EVENTS.x,	EVENTS.x},//LIMIT_UPPER
	};
	/* 状態遷移テーブルの宣言 */
	QUE_STATE[,] State_trans_tbl =
	{	//TAP_NO				//TOUCH					//SWIPE					//SWIPE_FAILED		//SWIPE_END			//SWING_END				//RETURN_END
		{QUE_STATE.STANDBY,     QUE_STATE.CONTROLED,    QUE_STATE.STANDBY,		QUE_STATE.STANDBY,	QUE_STATE.STANDBY,  QUE_STATE.STANDBY,      QUE_STATE.STANDBY},		//STANDBY(TAP_NO,TOUCH)
		{QUE_STATE.CONTROLED,   QUE_STATE.CONTROLED,    QUE_STATE.CONTROLED,	QUE_STATE.STANDBY,	QUE_STATE.SWING,    QUE_STATE.CONTROLED,    QUE_STATE.CONTROLED},	//CONTROLED(SWIPE,SWIPE_FAILED,SWIPE_END)
		{QUE_STATE.SWING,       QUE_STATE.SWING,        QUE_STATE.SWING,		QUE_STATE.SWING,	QUE_STATE.SWING,    QUE_STATE.RETURN,       QUE_STATE.SWING},		//SWING(SWING_END)
		{QUE_STATE.RETURN,      QUE_STATE.RETURN,       QUE_STATE.RETURN,		QUE_STATE.RETURN,	QUE_STATE.RETURN,   QUE_STATE.RETURN,       QUE_STATE.STANDBY},		//RETURN(RETURN_END)
	};

	/*==============================================================================================================================================================================*/
	/*↑ここまで宣言																																								*/
	/*==============================================================================================================================================================================*/

	/*==============================================================================================================================================================================*/
	/*↓ここから処理																																								*/
	/*==============================================================================================================================================================================*/

	/*==============================================================================================*/
	/* 関数名	:初期化処理																			*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:初期状態にて必要な設定をする(Que_state,Pos_origin_queの初期化						*/
	/*==============================================================================================*/
	void Start()
    {
		Que_state = QUE_STATE.STANDBY;//キューの初期状態は静止状態
		Pos_origin_que = Que.transform.position;//キューの初期位置を記憶
    }

	/*==============================================================================================*/
	/* 関数名	:メイン処理																			*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:update処理																			*/
	/*==============================================================================================*/
	// Update is called once per frame
	void FixedUpdate()
	{
		Pos_now_que = Que.transform.position;//キューの現在地を更新
		TAP_STATE _tap_state = control_tap();//タップ条件取得
		POS_STATE _pos_state = watch_pos_que();//位置条件取得
		EVENTS _que_event = set_event(_pos_state, _tap_state);//イベント決定処理
		
		if(_que_event != EVENTS.x)
		{	//有効なイベントが発行されていれば
			trans_state(_que_event);//状態遷移処理
		}
		action_by_state();//状態に応じたアクションの実行
	}

	/*==============================================================================================*/
	/* 関数名	:タップ条件取得																		*/
	/* 戻り値	:TAP_STATE tap_state																*/
	/* 引数		:なし																				*/
	/* 概要		:スマホ画面タップ時の処理															*/
	/* 役割		:タップ状態の取得、タップ位置の更新													*/
	/*==============================================================================================*/
	private TAP_STATE control_tap()
	{
		TAP_STATE tap_state;//戻り値用、タップによるイベントを格納
		tap_state = TAP_STATE.NO;//初期化

		if (Flg_touch)
		{   //タッチフラグがtrue -> 指が触れてる状態なら
			tap_state = TAP_STATE.KEEP;//タップ継続中イベントを格納
		}	/* この処理をタッチカウントのif文の後ろに置くと指が触れたとき、	*/
			/* tap_stateがtouchからkeepに上書きされてしまう					*/


		if (Input.touchCount > 0)   //タップされたら
		{
			foreach (Touch t in Input.touches)
			{
				if (t.phase == TouchPhase.Began) //タップしたとき
				{
					Flg_touch = true;//タッチフラグをtrueに
					tap_state = TAP_STATE.TOUCHED;//タップ開始(指が触れた)イベントを格納
					Pos_swipe_start = t.position.y;	//スワイプ開始時のタップ位置を記憶
				}

				if (t.phase == TouchPhase.Moved) //指の移動量があった時だけ呼ばれる
				{
					if(t.position.y < Pos_swipe_start)//画面下方向にスワイプしていれば
					{
						Pos_swipe_now = t.position.y;//画面タップ位置を取得
					}
				}

				if (t.phase == TouchPhase.Ended) //離したとき
				{
					Flg_touch = false;//タッチフラグをfalseに
					tap_state = TAP_STATE.RELEASE;//指が離れたイベントを格納
				}
			}
		}

		return tap_state;
	}
	/*==============================================================================================*/
	/* 関数名	:キューの位置確認処理																*/
	/* 戻り値	:POS_STATE pos_state																*/
	/* 引数		:																					*/
	/* 概要		:キューの位置から条件を決める														*/
	/*==============================================================================================*/
	private POS_STATE watch_pos_que()
	{
		POS_STATE pos_state;//戻り値用、キューの位置による状態を決める
		if( (Pos_origin_que.z- MARGIN_FOR_POS_ORIGIN) < Pos_now_que.z && Pos_now_que.z < (Pos_origin_que.z + MARGIN_FOR_POS_ORIGIN) )//初期位置の場合
		{	//幅を持たせたのはunity実行時にキューの初期位置がなぜかずれてしまうため
			pos_state = POS_STATE.ORIGIN;//キューの位置状態は初期位置
		}
		else if(Pos_now_que.z >= LIMIT_UPPER_POS_Y )//上限まで来てるとき
		{
			pos_state = POS_STATE.LIMIT_UPPER;//キューの位置状態は上限
		}
		else if(Que_state == QUE_STATE.RETURN && Pos_now_que.z < Pos_origin_que.z)//帰還しすぎているとき
		{
			pos_state = POS_STATE.ORIGIN;//無理やり原点扱い
		}
		else//それ以外の位置の場合
		{
			pos_state = POS_STATE.OTHER;//キューの位置は中途半端
		}
		return pos_state;
	}
	/*==============================================================================================================================*/
	/* 関数名	:イベント決定処理																									*/
	/* 戻り値	:EVENTS que_event																									*/
	/* 引数		:TAP_STATE tap_state, POS_STATE pos_state																			*/
	/* 概要		:キューに対するイベントを決定																						*/
	/*==============================================================================================================================*/
	private EVENTS set_event(POS_STATE _pos_state,TAP_STATE _tap_state)
	{
		EVENTS que_event = EVENTS.TAP_NO;//一応初期化

		/****************************範囲チェック******************************************/
		/**割り込みにより引数達が途中で値を変えられてしまうと怖いので自動変数に移す**/
		POS_STATE temp_pos_state = _pos_state;//_pos_stateの中身を自動変数に
		TAP_STATE temp_tap_state = _tap_state;//_tap_stateの中身を自動変数に

		//【要検討】_tap_stateが範囲外の値をとっていたらエラー処理
		//【要検討】_pos_stateが範囲外の値をとっていたらエラー処理
		//_tap_state,_pos_stateからイベント決定テーブルを参照しque_eventに代入
		switch (Que_state)
		{
			case QUE_STATE.STANDBY:
				que_event = Event_for_standby_tbl[(int)temp_pos_state, (int)temp_tap_state];
				break;
			case QUE_STATE.CONTROLED:
				que_event = Event_for_controled_tbl[(int)temp_pos_state, (int)temp_tap_state];
				break;
			case QUE_STATE.SWING:
				que_event = Event_for_swing_tbl[(int)temp_pos_state, (int)temp_tap_state];
				break;
			case QUE_STATE.RETURN:
				que_event = Event_for_return_tbl[(int)temp_pos_state, (int)temp_tap_state];
				break;
			default:
				break;
		}
		//現在の状態が帰還状態、かつ、キューが初期値なら
		//que_eventにRETURN_ENDを格納
		return que_event;
	}

	/*==============================================================================================================================*/
	/* 関数名	:状態遷移																											*/
	/* 戻り値	:なし																												*/
	/* 引数		:__que_event																										*/
	/* 概要		:キューの状態遷移のみ行う																							*/
	/*==============================================================================================================================*/
	private void trans_state(EVENTS __que_event)
	{
		Que_state = State_trans_tbl[(int)Que_state,(int)__que_event];//現在の状態と今回のイベントにより状態遷移
	}
	/*==============================================================================================================================*/
	/* 関数名	:アクション																											*/
	/* 戻り値	:なし																												*/
	/* 引数		:なし																												*/
	/* 概要		:状態に応じてキューを制御する																						*/
	/*==============================================================================================================================*/
	private void action_by_state()
	{
		switch (Que_state)
		{
			case QUE_STATE.STANDBY:
				standby_que();//静止処理実行
				break;
			case QUE_STATE.CONTROLED:
				control_que();//操作処理実行
				break;
			case QUE_STATE.SWING:
				swing_que();//スイング処理実行
				break;
			case QUE_STATE.RETURN:
				return_que();//帰還処理実行
				break;
		}
	}
	/*==============================================================================================================================*/
	/* 関数名	:静止処理																											*/
	/* 戻り値	:なし																												*/
	/* 引数		:なし																												*/
	/* 概要		:キューを静止する																									*/
	/*==============================================================================================================================*/
	private void standby_que()
	{
		Que.transform.position = new Vector3(Pos_origin_que.x, Pos_origin_que.y, Pos_origin_que.z);//キューを固定
		Rb_Que.isKinematic = true;//タップ中は力を受けさせない(これここにあっても意味なくない？？
		reset_que();
	}
	/*==============================================================================================================================*/
	/* 関数名	:操作処理																											*/
	/* 戻り値	:なし																												*/
	/* 引数		:なし																												*/
	/* 概要		:キューを操作する																									*/
	/*==============================================================================================================================*/
	private void control_que()
	{
		bool _flg_swipe = judge_swipe();//スワイプなのか同じところに指を置いているのかを判断
		if (_flg_swipe)
		{
			float _dis_move_que = convert_dis_swipe_to_que();//指の移動量をキューの移動量に変換
			move_que(_dis_move_que);//キュー移動処理をコール
		}
		Pos_swipe_before = Pos_swipe_now;//前回のタップ位置を更新
		Rb_Que.isKinematic = true;//タップ中は力を受けさせない(これここにあっても意味なくない？？
	}
	/*==============================================================================================================================*/
	/* 関数名	:スイング処理																										*/
	/* 戻り値	:なし																												*/
	/* 引数		:なし																												*/
	/* 概要		:キューをスイングする																								*/
	/*==============================================================================================================================*/
	private void swing_que()
	{
		float dis_moved_que = Pos_origin_que.z - Pos_now_que.z;//キュー原点と現在地の差を求める
		float force_que = BANE * dis_moved_que;//フックの法則よりキューに加える力を決定
		Rb_Que.isKinematic = false;//キューに力を印加できるように
		if(force_que > ZERO)
		{
			Rb_Que.AddForce(0, 0, force_que, ForceMode.Impulse);//キューに力を印加
		}
		else
		{	//無限ピストンを開始するとき
			Que_state = QUE_STATE.RETURN;//強制的に帰還状態へ遷移
		}
		
	}
	/*==============================================================================================================================*/
	/* 関数名	:帰還処理																											*/
	/* 戻り値	:なし																												*/
	/* 引数		:なし																												*/
	/* 概要		:キューを帰還させる																									*/
	/*==============================================================================================================================*/
	private void return_que()
	{
		Rb_Que.velocity = Vector3.zero;//印加している力を解除
		Que.transform.position = new Vector3(Pos_now_que.x, Pos_now_que.y, Pos_now_que.z - SPEED_RETURN);
	}

	/*==============================================================================================================================*/
	/* 関数名	:スワイプ判定関数																									*/
	/* 戻り値	:bool flg_swipe																										*/
	/* 引数		:なし																												*/
	/* 概要		:前回タップ位置と現在タップ位置の差が閾値以上ならスワイプと判断														*/
	/*==============================================================================================================================*/
	private bool judge_swipe()
	{
		bool flg_swipe;
		if ((Pos_swipe_now-Pos_swipe_before)>THRESHOLD_SWIPE || (Pos_swipe_before-Pos_swipe_now)>THRESHOLD_SWIPE)
		{
			flg_swipe = true;
		}
		else
		{
			flg_swipe = false;
		}
		return flg_swipe;
	}

	/*==============================================================================================================================*/
	/* 関数名	:移動量変換関数																										*/
	/* 戻り値	:float dis_move_que																									*/
	/* 引数		:なし																												*/
	/* 概要		:スワイプにおける指の移動量をキューの移動量に変換																	*/
	/*==============================================================================================================================*/
	private float convert_dis_swipe_to_que()
	{
		float dis_move_que;
		float dis_move_swipe = Pos_swipe_start - Pos_swipe_now;//スワイプ開始位置と現在のスワイプ位置の差を求める
		dis_move_que = SCALE_CONVERT * dis_move_swipe;//差をキューの移動量に変換
		return dis_move_que;
	}

	/*==============================================================================================================================*/
	/* 関数名	:キュー移動処理																										*/
	/* 戻り値	:なし																												*/
	/* 引数		:_dis_move_que																										*/
	/* 概要		:_dis_move_queだけキューを移動																						*/
	/*==============================================================================================================================*/
	private void move_que(float __dis_move_que)
	{
		if (LIMIT_LOWER_POS_Y <= Pos_now_que.z && Pos_now_que.z <= Pos_origin_que.z )//キューの位置が下限以上、原点以内なら
		{
			Que.transform.position = new Vector3(Pos_origin_que.x, Pos_origin_que.y , Pos_now_que.z - __dis_move_que);//キューを移動
		}
	}

	/*==============================================================================================================================*/
	/* 関数名	:キューリセット処理																									*/
	/* 戻り値	:なし																												*/
	/* 引数		:なし																												*/
	/* 概要		:内部変数をリセット																									*/
	/*==============================================================================================================================*/
	private void reset_que()
	{
		Pos_swipe_start = 0f; //スワイプ時のタップ開始位置を記憶
		Pos_swipe_before = 0f;    //スワイプ中の前回指が触れていた位置
		Pos_swipe_now = 0f;       //現在のスワイプ位置
	}
}
