﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSphere : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnCollisionEnter(Collision collision)
	{
		PlaySounds play_sounds = GameObject.Find("AudioPlayer").GetComponent<PlaySounds>();

		if (collision.gameObject.tag == "BASE")
		{
			play_sounds.Play_roling_sound();//転がる音
		}
		else
		{
			play_sounds.Play_hit_sound();//効果音
		}
	}
}
