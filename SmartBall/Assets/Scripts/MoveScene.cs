﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;//シーン遷移に必要

public class MoveScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	/*======================================================================*/
	/* 関数名	:Click_start_button											*/
	/* 戻り値	:															*/
	/* 引数		:なし														*/
	/* 概要		:【シーン遷移】タイトル→メイン								*/
	/*======================================================================*/
	public void Click_start_button()
	{
		SceneManager.LoadScene("MainScene");
	}

	/*======================================================================*/
	/* 関数名	:Click_move_result_button									*/
	/* 戻り値	:															*/
	/* 引数		:なし														*/
	/* 概要		:【シーン遷移】メイン→リザルト								*/
	/*======================================================================*/
	public void Click_move_result_button()
	{
		DataManager data_manager = GameObject.Find("Main Camera").GetComponent<DataManager>();
		data_manager.update_hi_score();

		SceneManager.LoadScene("ResultScene");
	}

	/*======================================================================*/
	/* 関数名	:Click_move_title_button									*/
	/* 戻り値	:															*/
	/* 引数		:なし														*/
	/* 概要		:【シーン遷移】リザルト→タイトル							*/
	/*======================================================================*/
	public void Click_move_title_button()
	{
		SceneManager.LoadScene("TitleScene");
	}
}
