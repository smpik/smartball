﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityForSphere : MonoBehaviour
{
	public float g = 9.8f;
	public Rigidbody Rb_Gravity;

	// Start is called before the first frame update
	void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
		GameObject[] spheres = GameObject.FindGameObjectsWithTag("SPHERE");
		foreach(GameObject sphere in spheres)
		{
			Rigidbody rb_sphere = sphere.GetComponent<Rigidbody>();
			Vector3 vec_direction = this.transform.position - sphere.transform.position;
			Vector3 force_gravity = g * vec_direction.normalized * (Rb_Gravity.mass * rb_sphere.mass) / (vec_direction.sqrMagnitude);
			rb_sphere.AddForce(0, 0, force_gravity.z);
		}
		
    }
}
