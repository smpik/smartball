﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayResult : MonoBehaviour
{
	private Text Text_result;//リザルトスコア表示用テキスト
	private Text Text_my_best;

	// Start is called before the first frame update
	void Start()
    {
		//DataManager data_manager = GameObject.Find("Main Camera").GetComponent<DataManager>();
		int _score = ControlBase.Get_score();//今回のスコアを取得

		Text_result = GameObject.Find("TextResult").GetComponent<Text>();
		Text_result.text = "今回のスコア：" + _score;

		int before_hi_score = ControlBase.Get_before_hi_score();
		Text_my_best = GameObject.Find("TextMyBest").GetComponent<Text>();
		Text_my_best.text = "自己ベスト：" + before_hi_score;
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
