﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	/*======================================================================*/
	/* 関数名	:update_hi_score											*/
	/* 戻り値	:なし														*/
	/* 引数		:なし														*/
	/* 概要		:ハイスコア更新(リザルトへ遷移時に呼び出される)				*/
	/*======================================================================*/
	public void update_hi_score()
	{
		//ControlBase control_base = GameObject.Find("Main Camera").GetComponent<ControlBase>();
		int credit = ControlBase.Get_score();//ControlBaseからG_scoreを取得

		int _hi_score = read_hi_score();//今までのハイスコアを取得
		if (credit > _hi_score)//今までのハイスコアと比較
		{	//最高記録が出たとき
			PlayerPrefs.SetInt("HiScore", credit);//ハイスコア更新
		}
		PlayerPrefs.Save();//保存(処理負荷大)
	}

	/*======================================================================*/
	/* 関数名	:read_hi_score												*/
	/* 戻り値	:int hi_score												*/
	/* 引数		:なし														*/
	/* 概要		:ハイスコア読出												*/
	/*======================================================================*/
	public int read_hi_score()
	{
		int hi_score = 0;
		hi_score = PlayerPrefs.GetInt("HiScore",0);//ハイスコア読み出し,デフォルト値は0
		return hi_score;
	}
}
