﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySounds : MonoBehaviour
{
	/*======================================================================*/
	/* 外部定数																*/
	/*======================================================================*/
	public AudioClip Hit_sound;//ボールを打つときの音
	public AudioClip Roling_sound;//ボールが転がるときの音

	/*======================================================================*/
	/* 内部定数																*/
	/*======================================================================*/
	private AudioSource Audio_player;//音を再生するオブジェクト

	// Start is called before the first frame update
	void Start()
    {
		Audio_player = GameObject.Find("AudioPlayer").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
		
    }

	/*======================================================================*/
	/* 関数名	:Play_hit_sound												*/
	/* 戻り値	:なし														*/
	/* 引数		:なし														*/
	/* 概要		:外部公開関数、ヒット時に呼ばれる							*/
	/*======================================================================*/
	public void Play_hit_sound()
	{
		Audio_player.PlayOneShot(Hit_sound, 0.5f);//第1引数が音源、第２が音量
	}

	/*======================================================================*/
	/* 関数名	:Play_roling_sound											*/
	/* 戻り値	:なし														*/
	/* 引数		:なし														*/
	/* 概要		:外部公開関数、Baseとヒット時に呼ばれる						*/
	/*======================================================================*/
	public void Play_roling_sound()
	{
		Audio_player.PlayOneShot(Roling_sound, 0.8f);//第1引数が音源、第２が音量
	}
}
