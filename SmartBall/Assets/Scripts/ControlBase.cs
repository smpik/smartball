﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlBase : MonoBehaviour
{
	public GameObject Sphere;
	public GameObject ConfirmCanvus;/*結果画面遷移確認画面*/
	public Vector3 Pos_Ball_Created;//ボール生成位置

	/*==============================================================================================*/
	/* 外部定数宣言																					*/
	/*==============================================================================================*/

	/* ホールのインデックスの宣言 */
	public enum HOLES
	{
		NUM_0 = 0,//NUM_0は普通の穴ではない
		NUM_1,
		NUM_2,
		NUM_3,
		NUM_4,
		NUM_5,
		NUM_6,
		NUM_7,
		NUM_8,
		NUM_9,
	}
	/*==============================================================================================*/
	/* 内部定数宣言																					*/
	/*==============================================================================================*/

	private const string x = "x";

	private enum BINGO
	{
		x = 0,	//陥没していない
		o,		//陥没している
	}

	/* ビンゴカードの宣言 */
	BINGO[,] Bingo_card =
	{
		{BINGO.x,	BINGO.x,	BINGO.x},//7, 8, 9
		{BINGO.x,	BINGO.x,	BINGO.x},//4, 5, 6
		{BINGO.x,	BINGO.x,	BINGO.x},//1, 2, 3
	};

	/*==============================================================================================*/
	/* 外部変数宣言																					*/
	/*==============================================================================================*/
	public static int G_Score;//今回のハイスコア保存用
	public static int G_Before_hi_score;//これまでのハイスコア(staticなのでシーン遷移しても保持される)

	/*==============================================================================================*/
	/* 内部変数宣言																					*/
	/*==============================================================================================*/
	private int Credit = 15;//スタート時のクレジットは15
	private Text Text_credit;//クレジットを表示するテキスト
	private string[] Sphere_names = { x, x, x, x, x, x, x, x, x,x };//陥没しているボールの名前保存用
																	//10個分用意しておかないとHOLES.NUM_9が使えない
	private string[] Names_bingo_sphere = { x, x, x, x, x, x, x, x, x, x }; //ビンゴしているボールの名前格納用
																	//10個分用意しておかないとHOLES.NUM_9が使えない
	private int Id_sphere = 0;//ボール識別番号
	private GameObject Que;//ControlQueクラスにアクセスするため
	private ControlQue Control_que;//ControlQueクラス内のメソッドを使用するため
	/*==============================================================================================*/
	/* 関数名	:初期化関数																			*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:初期設定																			*/
	/*==============================================================================================*/
	void Start()
    {
		G_Score = 0;//今回のスコアをリセット
		DataManager data_manager = GameObject.Find("Main Camera").GetComponent<DataManager>();
		G_Before_hi_score = data_manager.read_hi_score();//これまでのハイスコアを取得

		Que = GameObject.Find("Que");
		Control_que = Que.GetComponent<ControlQue>();//メソッドを使うため

		Text_credit = GameObject.Find("Text_credit").GetComponent<Text>();//Text_creditをヒエラルキーから取得
		Text_credit.text = Credit.ToString();//Text_creditにスタート時のCreditを表示
    }

	/*==============================================================================================*/
	/* 関数名	:メイン処理																			*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:update関数																			*/
	/*==============================================================================================*/
	void FixedUpdate()
    {
		
    }
	/*==============================================================================================*/
	/* 関数名	:ボール生成ボタンクリック															*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:ボタンタップでボール生成															*/
	/*==============================================================================================*/
	public void Click_Crete_Button()
	{
		bool _flg_permit = confirm_credit();//クレジット残高確認(関数コール)
		if (_flg_permit)
		{
			Id_sphere++;//ボール識別番号を更新
			GameObject sphere = Instantiate(Sphere, Pos_Ball_Created, Quaternion.identity);//あればボールを生成
			sphere.name = "sphere" + Id_sphere;//オブジェクトの名前に識別番号を付与
			Credit--;//クレジット減算
			update_credit();//クレジットテキストを更新
		}
		else
		{   /*残高ないのに押したら*/
			ConfirmCanvus.SetActive(true);/* 結果画面遷移確認を表示 */
		}
	}

	/*==============================================================================================*/
	/* 関数名	:ボール生成ボタンクリック															*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:ボタンタップでボール生成															*/
	/*==============================================================================================*/
	public void Click_stay_main_button()
	{
		ConfirmCanvus.SetActive(false);/* キャンバスを閉じる*/
	}

	/*==============================================================================================*/
	/* 関数名	:ゲーム終了ボタンクリック															*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:ボタンタタップで確認画面を開く														*/
	/*==============================================================================================*/
	public void Click_quit_button()
	{
		ConfirmCanvus.SetActive(true);/* キャンバスを開く*/
	}

	/*==============================================================================================*/
	/* 関数名	:精算ボタンクリック																	*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:ボタンタップで陥没ボール削除、精算													*/
	/*==============================================================================================*/
	public void Click_Payout_Button()
	{
		int bonus = 0;
		

		//ビンゴしていたらビンゴ処理
		if (Bingo_card[0, 0] == BINGO.o && Bingo_card[0, 0] == Bingo_card[0, 1] && Bingo_card[0, 1] == Bingo_card[0, 2])//0行目(横方向)
		{
			Names_bingo_sphere[(int)HOLES.NUM_7] = Sphere_names[(int)HOLES.NUM_7];//ビンゴしているボールの名前を記憶
			Names_bingo_sphere[(int)HOLES.NUM_8] = Sphere_names[(int)HOLES.NUM_8];
			Names_bingo_sphere[(int)HOLES.NUM_9] = Sphere_names[(int)HOLES.NUM_9];
			bonus += 15;//ボーナスを加算
		}
		if (Bingo_card[1, 0] == BINGO.o && Bingo_card[1, 0] == Bingo_card[1, 1] && Bingo_card[1, 1] == Bingo_card[1, 2])//1行目(横方向)
		{
			Names_bingo_sphere[(int)HOLES.NUM_4] = Sphere_names[(int)HOLES.NUM_4];//ビンゴしているボールの名前を記憶
			Names_bingo_sphere[(int)HOLES.NUM_5] = Sphere_names[(int)HOLES.NUM_5];
			Names_bingo_sphere[(int)HOLES.NUM_6] = Sphere_names[(int)HOLES.NUM_6];
			bonus += 15;//ボーナスを加算
		}
		if (Bingo_card[2, 0] == BINGO.o && Bingo_card[2, 0] == Bingo_card[2, 1] && Bingo_card[2, 1] == Bingo_card[2, 2])//2行目(横方向)
		{
			Names_bingo_sphere[(int)HOLES.NUM_1] = Sphere_names[(int)HOLES.NUM_1];//ビンゴしているボールの名前を記憶
			Names_bingo_sphere[(int)HOLES.NUM_2] = Sphere_names[(int)HOLES.NUM_2];
			Names_bingo_sphere[(int)HOLES.NUM_3] = Sphere_names[(int)HOLES.NUM_3];
			bonus += 15;//ボーナスを加算
		}
		if (Bingo_card[0, 0] == BINGO.o && Bingo_card[0, 0] == Bingo_card[1, 0] && Bingo_card[1, 0] == Bingo_card[2, 0])//0列目(縦方向)
		{
			Names_bingo_sphere[(int)HOLES.NUM_7] = Sphere_names[(int)HOLES.NUM_7];//ビンゴしているボールの名前を記憶
			Names_bingo_sphere[(int)HOLES.NUM_4] = Sphere_names[(int)HOLES.NUM_4];
			Names_bingo_sphere[(int)HOLES.NUM_1] = Sphere_names[(int)HOLES.NUM_1];
			bonus += 15;//ボーナスを加算
		}
		if (Bingo_card[0, 1] == BINGO.o && Bingo_card[0, 1] == Bingo_card[1, 1] && Bingo_card[1, 1] == Bingo_card[2, 1])//1列目(縦方向)
		{
			Names_bingo_sphere[(int)HOLES.NUM_8] = Sphere_names[(int)HOLES.NUM_8];//ビンゴしているボールの名前を記憶
			Names_bingo_sphere[(int)HOLES.NUM_5] = Sphere_names[(int)HOLES.NUM_5];
			Names_bingo_sphere[(int)HOLES.NUM_2] = Sphere_names[(int)HOLES.NUM_2];
			bonus += 15;//ボーナスを加算
		}
		if (Bingo_card[0, 2] == BINGO.o && Bingo_card[0, 2] == Bingo_card[1, 2] && Bingo_card[1, 2] == Bingo_card[2, 2])//2列目(縦方向)
		{
			Names_bingo_sphere[(int)HOLES.NUM_9] = Sphere_names[(int)HOLES.NUM_9];//ビンゴしているボールの名前を記憶
			Names_bingo_sphere[(int)HOLES.NUM_6] = Sphere_names[(int)HOLES.NUM_6];
			Names_bingo_sphere[(int)HOLES.NUM_3] = Sphere_names[(int)HOLES.NUM_3];
			bonus += 15;//ボーナスを加算
		}
		if (Bingo_card[0, 0] == BINGO.o && Bingo_card[0, 0] == Bingo_card[1, 1] && Bingo_card[1, 1] == Bingo_card[2, 2])//「\」(クロス)
		{
			Names_bingo_sphere[(int)HOLES.NUM_7] = Sphere_names[(int)HOLES.NUM_7];//ビンゴしているボールの名前を記憶
			Names_bingo_sphere[(int)HOLES.NUM_5] = Sphere_names[(int)HOLES.NUM_5];
			Names_bingo_sphere[(int)HOLES.NUM_3] = Sphere_names[(int)HOLES.NUM_3];
			bonus += 15;//ボーナスを加算
		}
		if (Bingo_card[0, 2] == BINGO.o && Bingo_card[0, 2] == Bingo_card[1, 1] && Bingo_card[1, 1] == Bingo_card[2, 0])//「/」(クロス)
		{
			Names_bingo_sphere[(int)HOLES.NUM_9] = Sphere_names[(int)HOLES.NUM_9];//ビンゴしているボールの名前を記憶
			Names_bingo_sphere[(int)HOLES.NUM_5] = Sphere_names[(int)HOLES.NUM_5];
			Names_bingo_sphere[(int)HOLES.NUM_1] = Sphere_names[(int)HOLES.NUM_1];
			bonus += 15;//ボーナスを加算
		}
		
		bonus += ((bonus-15) / 15) * bonus;//複数ビンゴしていれば更にボーナス
		add_credit(bonus);//クレジットにボーナスを加算
		clear_sphere_fallen();//陥没ボールの削除
	}

	/*==============================================================================================*/
	/* 関数名	:陥没ボール削除																		*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:陥没ボールを削除																	*/
	/*==============================================================================================*/
	private void clear_sphere_fallen()
	{
		for (int i = (int)HOLES.NUM_1; i <= (int)HOLES.NUM_9; i++)//[HOLES.NUM_0]は無視
		{
			if (Names_bingo_sphere[i] != x)
			{   //名前が入ってれば
				Destroy(GameObject.Find(Sphere_names[i]));//該当する名前のボールを削除
				Names_bingo_sphere[i] = x;//ビンゴボール名保存用リストから名前を消去
				Sphere_names[i] = x;//陥没ボール名保存用リストからも名前を消去
			}
		}
	}
	/*==============================================================================================*/
	/* 関数名	:クレジット残高確認																	*/
	/* 戻り値	:bool flg_permit																	*/
	/* 引数		:なし																				*/
	/* 概要		:クレジットがあればボール生成許可を与える											*/
	/*==============================================================================================*/
	private bool confirm_credit()
	{
		bool flg_permit = false;//戻り値の定義

		if (Credit > 0)//クレジットがあれば
		{
			flg_permit = true;//ボール生成許可を与える
		}

		return flg_permit;
	}
	/*==============================================================================================*/
	/* 関数名	:クレジットテキスト更新																*/
	/* 戻り値	:なし																				*/
	/* 引数		:なし																				*/
	/* 概要		:クレジットを更新する																*/
	/*==============================================================================================*/
	private void update_credit()
	{
		Text_credit.text = Credit.ToString();
	}
	/*==============================================================================================*/
	/* 関数名	:陥没検知																			*/
	/* 戻り値	:なし																				*/
	/* 引数		:HOLES num_hole																		*/
	/* 概要		:ボールが穴に入ったらクレジットを加算												*/
	/*==============================================================================================*/
	public void Sense_sphere_fallen(HOLES num_hole, Collision collision)
	{
		int income;

		if(num_hole >= HOLES.NUM_1)
		{   //穴に入った時
			PlaySounds play_sounds = GameObject.Find("AudioPlayer").GetComponent<PlaySounds>();
			play_sounds.Play_roling_sound();//rolingなのはもう気にしない（いい感じの音だから）
			income = 5;//加算量の決定←陥没した穴(numhole)に応じてもいいかも
			Sphere_names[(int)num_hole] = collision.gameObject.name;//陥没位置と名前を記憶
			control_bingo(num_hole);//ビンゴカードに穴をあける
			add_credit(income);//クレジットの加算
		}
		else
		{	//hole0にあたったとき
			trash_sphere(collision);//ボール回収
		}
	}
	/*==============================================================================================*/
	/* 関数名	:クレジット加算																		*/
	/* 戻り値	:なし																				*/
	/* 引数		:int income																			*/
	/* 概要		:ボールが穴に入ったらクレジットを加算												*/
	/*==============================================================================================*/
	private void add_credit(int income)
	{
		Credit += income;
		G_Score = Credit;//ハイスコアを記憶
		update_credit();//クレジットテキストの更新
	}

	/*==============================================================================================*/
	/* 関数名	:ビンゴカード処理																	*/
	/* 戻り値	:なし																				*/
	/* 引数		:HOLES num_hole																		*/
	/* 概要		:ビンゴカードの穴開け、ビンゴ処理、(リーチも?)										*/
	/*==============================================================================================*/
	private void control_bingo(HOLES num_hole)
	{
		//穴あけ処理
		switch (num_hole)						// 7 8 9	→	[0,0] [0,1] [0,2]
		{										// 4 5 6	→	[1,0] [1,1] [1,2]
			case HOLES.NUM_1:					// 1 2 3	→	[2,0] [2,1] [2,2]
				Bingo_card[2,0] = BINGO.o;
				break;
			case HOLES.NUM_2:
				Bingo_card[2,1] = BINGO.o;
				break;
			case HOLES.NUM_3:
				Bingo_card[2,2] = BINGO.o;
				break;
			case HOLES.NUM_4:
				Bingo_card[1,0] = BINGO.o;
				break;
			case HOLES.NUM_5:
				Bingo_card[1,1] = BINGO.o;
				break;
			case HOLES.NUM_6:
				Bingo_card[1,2] = BINGO.o;
				break;
			case HOLES.NUM_7:
				Bingo_card[0,0] = BINGO.o;
				break;
			case HOLES.NUM_8:
				Bingo_card[0,1] = BINGO.o;
				break;
			case HOLES.NUM_9:
				Bingo_card[0,2] = BINGO.o;
				break;
		}
	}

	/*==============================================================================================*/
	/* 関数名	:ボール回収(クレジットを増やすわけではない)											*/
	/* 戻り値	:なし																				*/
	/* 引数		:Collision collision																*/
	/* 概要		:ボールの消去																		*/
	/*==============================================================================================*/
	private void trash_sphere(Collision collision)
	{
		Destroy(collision.gameObject);
	}

	/*==============================================================================================*/
	/* 関数名	:これまでハイスコア送信																*/
	/* 戻り値	:int G_Before_hi_score																*/
	/* 引数		:なし																				*/
	/* 概要		:外部公開関数																		*/
	/*==============================================================================================*/
	public static int Get_before_hi_score()
	{
		return G_Before_hi_score;
	}

	/*==============================================================================================*/
	/* 関数名	:今回のスコア送信																	*/
	/* 戻り値	:int G_score																		*/
	/* 引数		:なし																				*/
	/* 概要		:外部公開関数																		*/
	/*==============================================================================================*/
	public static int Get_score()
	{
		return G_Score;
	}
}